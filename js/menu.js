(function ($) {
  $( document ).ready(function() {
    $('#simple-menu').sidr({
      name: 'sidr-mobile',
      source: '#sidr-main',
      onOpen: function () {
        $('#simple-menu').addClass('is-active');
      },
      onClose: function () {
        $('#simple-menu').removeClass('is-active');
      },
    });
    $('.sidr-class-mobile-close').click(function() {
      $.sidr('close', 'sidr-mobile');
    });
  });
}(jQuery));
