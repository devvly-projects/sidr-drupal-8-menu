# Intro
This is example code for implamenting the Sidr menu in a custom Drupal 8 theme.
https://www.berriart.com/sidr/

# Instructions
- The example files only include the code needed for the menu, so you can't simply replace your theme files with them.
- This should only be used if it's a flat menu with no child menu items. If you need a menu with child items use MultiLevelPushMenu https://gitlab.com/devvly-projects/mlpushmenu
- The *customtheme* name will need to be replaced with you custom theme everywhere it's used.